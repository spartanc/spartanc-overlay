# spartanc-overlay

This is a [Gentoo](https://gentoo.org) ebuild overlay used to build container images for `spartanc` (spartan container). It is based on the [kubler](https://github.com/edannenberg/kubler/) build tool.

The intention is not create minimalstic and hardended (hence "spartan") container images of common tools. Although the image building process is based on Gentoo, the resulting images are expected to be distribution netural. Much like the [distroless]() and [Chainguard Image]() projects, the aim is to have container images free of distro-specific bloat.

Ebuilds published here are either not part of the main portage tree or a modified version of the ebuild (usually introducing `static` support) is required to achieve aboce mentioned goal.
